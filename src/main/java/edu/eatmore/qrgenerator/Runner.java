package edu.eatmore.qrgenerator;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.EnumMap;
import java.util.Map;

public class Runner {

  private static final Charset CHARSET = Charset.forName("ISO-8859-15");

  private static final int QR_SIZE = 165;

  private static final String TEXT = "Hello World!";

  public static void main(String[] args) throws Exception {
    String text = new String(TEXT.getBytes(CHARSET), CHARSET);
    QRCodeWriter qrCodeWriter = new QRCodeWriter();
    Map<EncodeHintType, Object> hints = new EnumMap<>(EncodeHintType.class);
    hints.put(EncodeHintType.CHARACTER_SET, CHARSET);
    hints.put(EncodeHintType.MARGIN, 0);
    BitMatrix bitMatrix = qrCodeWriter.encode(text, BarcodeFormat.QR_CODE, QR_SIZE, QR_SIZE, hints);
    BufferedImage bufferedImage = MatrixToImageWriter.toBufferedImage(bitMatrix);
    ImageIO.write(bufferedImage, "PNG", new File("buffered_image.png"));
    MatrixToImageWriter.writeToPath(bitMatrix, "PNG", Paths.get("generated_QR.png"));
    String base64 = new String(Base64.getEncoder().encode(Files.readAllBytes(Paths.get("generated_QR.png"))));
    System.out.println(base64);
  }
}
